\documentclass{beamer}
\mode<presentation> {

% The Beamer class comes with a number of default slide themes
% which change the colors and layouts of slides. Below this is a list
% of all the themes, uncomment each in turn to see what they look like.

%\usetheme{default}
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{Dresden}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}
}
\usepackage{lmodern}
\usepackage{float}
\usepackage{subfloat}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=1.12}
\usetikzlibrary{matrix}
\usepgfplotslibrary{groupplots}
\pgfplotsset{compat=newest}
\usepackage{mathtools}
\usepackage{booktabs}
\usepackage{multicol}

\def\insop{I\scriptsize{NSERT}}
\def\tryinsop{T\scriptsize{RY}\normalsize{I}\scriptsize{NSERT}}
\def\delop{D\scriptsize{ELETE}}
\def\trydelop{T\scriptsize{RY}\normalsize{D}\scriptsize{ELETE}}
\def\getop{G\scriptsize{ET}}
\def\seaop{S\scriptsize{EARCH}}
\def\iniop{I\scriptsize{NIT}}
\def\cleop{C\scriptsize{LEANUP}}
\def\tryrbop{T\scriptsize{RY}\normalsize{R}\scriptsize{EBALANCE}}
\def\proop{P\scriptsize{ROMOTE}}
\def\rotop{R\scriptsize{OTATE}}
\def\dbrotop{\normalsize{D}\scriptsize{OUBLE}\normalsize{R}\scriptsize{OTATE}}
\def\temop{T\scriptsize{EMPLATE}}
\def\conop{\normalsize{C}\scriptsize{ONDITION}\normalsize{M}\scriptsize{ET}}
\def\nexop{\normalsize{N}\scriptsize{EXT}\normalsize{N}\scriptsize{ODE}}
\def\scxop{\normalsize{S}\scriptsize{CX}\normalsize{A}\scriptsize{RGUMENT}}
\def\resop{R\scriptsize{ESULT}}

\pgfplotscreateplotcyclelist{ravl_vs_unbalanced}{%
solid, mark options={solid, fill=white}, mark=triangle*\\% lf-nbst
solid, mark options={solid, fill=white}, mark=diamond*\\% lf-ebst
solid, mark options={solid, fill=white},mark=*\\% lf-ibst
solid, mark=star\\% lf-cast
solid, mark options={solid, fill=black}, mark=x\\% lf-citr
dashed, mark options={fill=black}, mark=*\\%
solid, mark options={solid, fill=black}, mark=*\\%
}

\pgfplotscreateplotcyclelist{ravl_vs_balanced}{%
dashed, mark options={solid, fill=white}, mark=square*\\%
solid, mark options={solid, fill=white}, mark=square*\\%
solid, mark options={solid, fill=white}, mark=otimes*\\%
dashed, mark options={fill=black}, mark=*\\%
solid, mark options={solid, fill=black}, mark=*\\%
}

\title{Deletion Without Rebalancing in Non-Blocking Self-Balancing Binary Search
Trees} 
\author[Mengdu Li]{Mengdu Li\\{\small Supervised by: Dr. Meng He}}

\institute
{
Dalhousie University\\
\medskip
}
\date{April, 2016} 

\begin{document}

\begin{frame}
\titlepage 
\end{frame}

% \begin{frame}
% \frametitle{Overview}
% \tableofcontents
% \end{frame}

\section{Objectives}

\subsection{Problem}
\begin{frame}
\frametitle{Problem}
\begin{enumerate}
  \item Lack of studies of non-blocking self-balancing {\em Binary Search Tree}
  (BST).
  \item Better experiments needed to evaluate the performance of self-balancing
  BSTs.
  \begin{enumerate}
    \item Only random data are used.
    \item Favor BSTs that do not support self-balancing.
    \item In many real-world applications, data are partially sorted.
  \end{enumerate}
  \item Self-balancing BSTs are hard to implement in practice.
  \begin{enumerate}
    \item Introduce a large number of rebalancing cases
    after {\delop} operations.
    \item Hard to make each case robust in practice.
  \end{enumerate}
\end{enumerate}
\end{frame}

\subsection{Goals}
\begin{frame}
\frametitle{Goals}
We propose the {\em non-blocking ravl tree}.
\begin{enumerate}
  \item Based on the {\em ravl tree} proposed by Sen and Tarjan~\cite{Sen:2010}.
  \item Do not rebalance itself after {\delop} operations.
  \item Introduce only $5$ rebalancing cases.
  \item Provably correct, linearizable and non-blocking.
  \item Provable non-trivial bound on its height.
  \item Experimental studies to evaluate its performance.
  \begin{enumerate}
    \item Random data.
    \item Data sequences that are partially sorted.
  \end{enumerate}
\end{enumerate}

\end{frame}

\section{Preliminaries}
\subsection{LLX and SCX Operations}
\begin{frame}
\frametitle{Preliminaries: LLX and SCX Operations~\cite{Brown:2013}}
{\em Data-record}:
\begin{enumerate}
  \item Mutable fields.
  \item Immutable fields.
\end{enumerate}
{\em Load-Linked Extended} (LLX):
\begin{enumerate}
  \item Input: Data-record $r$.
  \item Output:
  \begin{enumerate}
    \item Values of $r$'s mutable fields.
    \item $finalized$.
    \item $fail$.
  \end{enumerate}
\end{enumerate}
% \end{frame}
% \begin{frame}
% \frametitle{Preliminaries: LLX and SCX Operations~\cite{Brown:2013} (ctd.)}
{\em Store-Condition Extended} (SCX): atomically update the data structure
\begin{enumerate}
  \item Input: $V,R, fld, new$
  \item Output:
  \begin{enumerate}
    \item $(*fld) \gets new$; Remove $R$
    \item $fail$
  \end{enumerate}
\end{enumerate}
\end{frame}

\subsection{Tree Update Template}
\begin{frame}
\frametitle{Preliminaries: Tree Update Template~\cite{Brown:2014}}
A template for efficient non-blocking implementation of update operations in
{\em down tree} (do not maintain parent pointers).
\begin{enumerate}
  \item Each node is a Data-record.
  \item Mutable fields: child pointers.
  \item Immutable fields: other fields.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Preliminaries: Tree Update Template~\cite{Brown:2014} (ctd.)}
\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{template}

\caption{An example of a tree update operation following the
template in
\cite{Brown:2014}.}
\label{fig:tue}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Preliminaries: Tree Update Template~\cite{Brown:2014} (ctd.)}
An update operation following the tree update template:
\begin{enumerate}
  \item Perform a top-down traversal starting from the root of the tree until
 $parent$ is reached.
  \item Perform a sequence of LLX operations on $parent$ and a contiguous set of
  its descendants related to the desired update.
  \item Construct $V$ from the set of nodes visited.
  \item Construct $R$ (subset of $V$).
  \item Create $N$.
  \item Let pointer $fld$ point to the child field of $parent$ to be changed.
  \item SCX($V, R, fld, new$).
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Preliminaries: Tree Update Template~\cite{Brown:2014} (ctd.)}
\begin{lemma}[\cite{Brown:2014}] \label{th:temp_cons}
Successful update operations following the tree update template are linearizable
and non-blocking.
\end{lemma} 
\end{frame}

\section{Non-Blocking Ravl Trees}
\subsection{Structure of Non-Blocking Ravl Trees}
\begin{frame}
\frametitle{Structure of Non-Blocking Ravl Trees}
\begin{enumerate}
  \item External tree.
  \item Each node is a Data-record:
  \begin{enumerate}
    \item Mutable fields: $left$, $right$.
    \item Immutable fields: $key$, $value$, $rank$.
  \end{enumerate}
%   \item {\em entry} node: entry point of the tree.
%   \item {\em sentinel} nodes: always present in the tree ($key=\infty,
%   value=NULL, rank=\infty$).
  \item {\em missing} nodes: conceptually added to leaves ($rank=-1$).
\end{enumerate}
\end{frame}

% \begin{frame}
% \frametitle{Structure of Non-Blocking Ravl Trees (ctd.)}
% \begin{figure}
% \centering
% \begin{subfigure}[t]{0.45\textwidth}
% \centering
% \caption{An empty non-blocking ravl tree.}
% \includegraphics[scale=0.22]{empty}
% \end{subfigure}
% ~
% \begin{subfigure}[t]{0.45\textwidth}
% \centering
% \caption{A non-empty non-blocking ravl tree.}
% \includegraphics[scale=0.22]{non_empty}
% \end{subfigure}
% ~
% \caption{The structures of an empty and a non-empty non-blocking ravl
% tree.}
% \label{fig:seq_stuct}
% \end{figure}
% {\em root}: leftmost grandchild of the entry node.
% 
% {\em original} nodes: any node that is not a sentinel node nor a
% missing node ($rank \geq 0$).
% \end{frame}

\begin{frame}
\frametitle{Structure of Non-Blocking Ravl Trees (ctd.)}
For any node $x$ whose parent is $z$:
\begin{enumerate}
  \item $x$ is a {\em $i$-node} if $z.r - x.r = i$.
  \item $x$ is an {\em $i,j$-node} if one of $x$'s children is an $i$-node and
  the other is a $j$-node.
  \item If $x$ is a $0$-node:
  \begin{enumerate}
    \item $x$ is a {\em violation}.
    \item Edge $(z, x)$ is a {\em violating edge}.
%     \item $z$ is a $0$-parent.
  \end{enumerate}
\end{enumerate}
% Violations can be created by {\insop} operations and rebalancing operations.
% 
% A non-blocking ravl tree is balanced after all violations have been resolved.
\end{frame}

\subsection{Operations in Non-Blocking Ravl Trees}
\begin{frame}
\frametitle{Operations in Non-Blocking Ravl Trees}
{\getop}($key$):
\begin{enumerate}
  \item BST search.
  \item Wait-free.
  \item Output:
  \begin{enumerate}
    \item Value associated.
    \item $Null$.
  \end{enumerate}
%   {\getop}($key$) returns the value stored in the node identified by $key$
%   if such node exists in the tree; and $NULL$ otherwise.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Operations in Non-Blocking Ravl Trees (ctd.)}
{\insop}($key, value$):\\
repeat: \\
\quad Search for $key$ and get leaf $l$ \\
\quad If $l.key = key$ return $false$ \\
\quad Try to insert $key$ following tree update template \\
\quad If insertion fails, retry \\
\quad If insertion succeeds \\
\qquad If new violations have been created, call {\cleop}($key$)\\
\qquad return $true$
\end{frame}
\begin{frame}
\frametitle{Operations in Non-Blocking Ravl Trees (ctd.)}

% \begin{multicols}{2}
\begin{figure}
\centering
\includegraphics[scale=0.20]{nb_insert}
\caption{{\insop} $key$.}
\end{figure}
% Replace $l$ with new subtree rooted at $new$ (assume $key < l.key$).
\begin{enumerate}
  \item $new.k = l.key$, $new.v = NULL$, $new.r = l.r$
  \item $new\_k.k = key$, $new\_k.v = value$, $new\_k.r = 0$
  \item $new\_l.k = l.key$, $new\_l.v = l.v$, $new\_l.r = 0$
\end{enumerate}
% \end{multicols}

\end{frame}

\begin{frame}
\frametitle{Operations in Non-Blocking Ravl Trees (ctd.)}
{\delop}($key$):\\
repeat: \\
\quad Search for $key$ and get leaf $l$ \\
\quad If $l.key \neq key$ return $false$ \\
\quad Try to delete $key$ following tree update template \\
\quad If deletion fails, retry \\
\quad If deletion succeeds, return $true$
\begin{figure}
\centering
\includegraphics[scale=0.28]{nb_delete}
\caption{{\delop} $key$.}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Operations in Non-Blocking Ravl Trees (ctd.)}
{\cleop}($key$):\\
repeat: \\
\quad Search for $key$ \\
\quad If a leaf is reached (no violation on the path), return \\
\quad If a violation is found \\
\qquad Choose a rebalancing operation \\
\qquad Try to resolve the violation following the tree update template \\
\qquad Restart the search
\end{frame}

\begin{frame}
\frametitle{Resolving Violation $x$}
Case $1$ {\proop}:
\begin{figure}
\centering
\includegraphics[scale=0.18]{nb_promote}
\caption{Case $1$ {\proop} on node $z$.}
\end{figure}
$z$ is a $0$,$0$-node. \\
Replace $z$ with $new\_z$ where $new\_z.r = z.r + 1$.
\end{frame}

\begin{frame}
\frametitle{Resolving Violation $x$ (ctd.)}
Case $2$ {\proop}:
\begin{figure}
\centering
\includegraphics[scale=0.26]{nb_promote2}
\caption{Case $2$ {\proop} on node $z$.}
\end{figure}
$z$ is a $0$,$1$-node. \\
Replace $z$ with $new\_z$ where $new\_z.r = z.r + 1$.
\end{frame}

\begin{frame}
\frametitle{Resolving Violation $x$ (ctd.)}
Case $1$ {\rotop}:
\begin{figure}
\centering
\includegraphics[scale=0.3]{nb_rotate}
\caption{Case $1$ {\rotop} on node $x$.}
\end{figure}
$z$ is a $0$,$i$-node, where $i\geq 2$. \\
% Assume $x$ is $z$'s left child. Then $y$ is $x$'r right child, $y_s$ is $x$'s
% left child.\\
$x.r \geq y.r + 2$ or $y$ is a missing node.\\
 Replace $x$ and $z$ with $new\_x$ and $new\_z$ where $new\_x.r = x.r$ and
 $new\_z.r = z.r - 1$.
\end{frame}

\begin{frame}
\frametitle{Resolving Violation $x$ (ctd.)}
Case $2$ {\rotop}:
\begin{figure}
\centering
\includegraphics[scale=0.25]{nb_rotate2}
\caption{Case $2$ {\rotop} on node $x$.}
\end{figure}
$z$ is a $0$,$i$-node, where $i\geq 2$. \\
$x$ is a $1$,$1$-node.\\
 Replace $x$ and $z$ with $new\_x$ and $new\_z$ where $new\_x.r = x.r + 1$ and
 $new\_z.r = z.r$.
\end{frame}

\begin{frame}
\frametitle{Resolving Violation $x$ (ctd.)}
{\dbrotop}:
\begin{figure}
\centering
\includegraphics[scale=0.2]{nb_double_rotate}
\caption{{\dbrotop} on node $y$.}
\end{figure}
$z$ is a $0$,$i$-node, where $i\geq 2$. \\
$x.r = y.r + 1$ and $x.r \geq y_s.r + 2$.\\
 Replace $x$, $y$ and $z$ with $new\_x$, $new\_y$ and $new\_z$ where $new\_x.r =
 x.r - 1$, $new\_y.r = y.r+1$ and $new\_z.r = z.r - 1$.
\end{frame}
\section{Correctness of Non-Blocking Ravl Trees}
% \begin{frame}
% \frametitle{Correctness of Non-Blocking Ravl Trees}
% 
% \end{frame}
\begin{frame}
\frametitle{Correctness of Non-Blocking Ravl Trees}
\begin{lemma}
All possible violation cases in non-blocking ravl trees can be resolved by the
rebalancing operations.
\end{lemma}
\begin{lemma}\label{th:bst_anytime}
A non-blocking ravl tree remains a BST at any time.
\end{lemma}
\begin{enumerate}
%   \item Sentinel nodes are not affected by update operations.
  \item The BST property is preserved for all existing nodes.
  \item The BST property is preserved for all new nodes added by update
    operations.
\end{enumerate}
\end{frame}
\section{Linearizability of Non-Blocking Ravl Trees}
\begin{frame}
\frametitle{Linearizability of Non-Blocking Ravl Trees}
\begin{lemma} \label{th:nb_linearization}%keep
% A non-blocking ravl tree is linearizable, and the linearization points of its
% operations are defined as follows: 

\begin{enumerate}
  \item A {\seaop} operation is linearized when it reaches a leaf.
  \item A {\getop} operation is linearized at the linearization point of its
  {\seaop} step. 
  \item If an {\insop} operation returns $true$, it follows the tree update
  template, and is thus linearizable.
  \item If an {\insop} operation returns $false$, it is linearized at the linearization point of its
  {\seaop} step. 
  \item  If a {\delop} operation returns $true$,  it follows the tree update
  template, and is thus linearizable.
  \item If a {\delop} operation returns
  $false$, it is linearized at the linearization point of its
  {\seaop} step. 
\end{enumerate}
\end{lemma}
\end{frame}

\section{Progress Properties of Non-Blocking Ravl Trees}
\begin{frame}
\frametitle{Progress Properties of Non-Blocking Ravl Trees}
\begin{lemma}
All operations in non-blocking ravl trees are non-blocking.
\end{lemma}
\begin{enumerate}
  \item Read operations are wait-free.
  \item All read operations will eventually terminate.
  \item All update operations follow the tree update template, and thus are
  non-blocking.
  \item The number of successful updates is finite, and thus {\insop} and
  {\delop} operations will eventually terminate.
\end{enumerate}
\end{frame}

\section{Bounding the Tree Height}
\begin{frame}
\frametitle{Bounding the Tree Height}

% \end{frame}
% \begin{frame}
% \frametitle{Bounding the Tree Height (ctd.)}
At any time $t$:
\begin{enumerate}
  \item $m$: number of insertions that inserted keys.
  \item $c$: number of active processes that are rebalancing the tree.
% \end{enumerate}
% 
% \begin{enumerate}

  \item The number of violating edges on each root-to-leaf path is
  bounded by $c$.
  \item The number of non-violating edges on each root-to-leaf path
  is bounded by $\log_\phi (2m)$.
  \item The height of the ravl tree is bounded by
  $\log_\phi (2m) + c$.
\end{enumerate}

\end{frame}
\section{Experiments}
\section{Experimental Settings}
\begin{frame}
\frametitle{Compared Algorithms}
\begin{enumerate}
\item \textbf{lf-ravl}, the non-blocking ravl tree.
\item \textbf{lf-ravl3}, a variant of lf-ravl that allows at most three
violations on each root-to-leaf path.
\item \textbf{lf-chrm}, the non-blocking chromatic tree~\cite{Brown:2014}.
\item \textbf{lf-chrm6}, a variant of lf-chrm
that allows at most six violations on
each root-to-leaf path~\cite{Brown:2014}.
\item \textbf{lc-davl}, lock-based AVL tree~\cite{Drachsler:2014}.
\item \textbf{lf-nbst}, non-blocking external BST~\cite{Natarajan:2014}.
\item \textbf{lf-ebst}, non-blocking external BST~\cite{Ellen:2010}.
\item \textbf{lf-ibst}, non-blocking internal BST~\cite{Ramachandran:2015(1)}.
\item \textbf{lc-cast}, lock-based BST~\cite{Ramachandran:2015(2)}.
\item \textbf{lc-citr}, lock-based BST~\cite{Arbel:2014}.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Experimental Settings}
\begin{enumerate}
  \item Performance is measured by average throughput.
  \item Experimental studies using randomly generated data
  \begin{enumerate}
    \item Number of threads: 1, 2, 4, 8, 16 and 32
    \item Different key ranges (contention levels).
    \item Different update ratios.
  \end{enumerate}
%   \item Experimental studies using data sequences with different degrees of
%   presortedness.
%   \begin{enumerate}
%     \item Number of threads: 32
%     \item Different degrees of presortedness.
%     \item Different update ratio.
%   \end{enumerate}
\end{enumerate}
\end{frame}
\section{Experimental Results}
\begin{frame}[fragile]
\frametitle{Performance under Different Contention Levels}
% Our solution is more contention-friendly compared to other
% concurrent self-balancing BSTs.
\begin{figure}
\begin{tikzpicture}
    \begin{groupplot}[group style={group size= 2 by 1},width=.45\textwidth]
        \nextgroupplot[title={High contention},cycle list name=ravl_vs_balanced,
        smooth ,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}
    ] %r4 90
    \addplot coordinates {
(1, 4335883.2)
(2, 6543535.29)
(4, 12533295.8)
(8, 23421046.39)
(16, 40836017)
(32, 63867747.95)
    };\label{plots:lf-chrm}
        \addplot coordinates {
(1, 4557514)
(2, 7022379.72)
(4, 13347123.78)
(8, 24612559)
(16, 45238448.11)
(32, 70224190.49)
    };\label{plots:lf_chrm6}
    \addplot coordinates {
(1, 5416987)
(2, 5486593.88)
(4, 10732465.71)
(8, 19960144.8)
(16, 37466168.57)
(32, 54462468.52)
    };\label{plots:lc-davl}
     \addplot plot coordinates {
(1, 4601500.2)
(2, 7103966.4)
(4, 13760212.36)
(8, 25438531.09)
(16, 42388413.72)
(32, 65392302.42)
    };\label{plots:lf-ravl}
     \addplot plot coordinates {
(1, 4672694.06)
(2, 7456583.4)
(4, 14108320.54)
(8, 26380043.99)
(16, 48193100.58)
(32, 72029847.69)
    };\label{plots:lf-ravl3}
    \coordinate (top) at (rel axis cs:0,1);
    \nextgroupplot[title={Low contention},cycle list name=ravl_vs_balanced,
    smooth,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}] %r4 70
    \addplot coordinates {
(1, 1155050.99)
(2, 2575555.2)
(4, 4860523.5)
(8, 9041855.83)
(16, 17042757.2)
(32, 28617906.26)
    }; %chrm
    
    \addplot coordinates {
(1, 949388.12)
(2, 2430329)
(4, 4639289.54)
(8, 8872601.88)
(16, 16655465.91)
(32, 28970834.1)
    }; %chrm6
    \addplot coordinates {
(1, 913215)
(2, 2200250.95)
(4, 4210641.67)
(8, 7990627.2)
(16, 14993778.84)
(32, 27229718.97)
    }; %davl
    \addplot coordinates {
(1, 1143676.8)
(2, 2659757.65)
(4, 5068947.2)
(8, 9625629.47)
(16, 18463488.7)
(32, 30513425.63)
    }; %ravl
    
    \addplot coordinates {
(1, 992804.44)
(2, 2527660)
(4, 4864004.4)
(8, 9193685.66)
(16, 18681980.01)
(32, 31081770.89)
    }; %ravl3

    \coordinate (bot) at (rel axis cs:1,0);% coordinate at bottom of the last plot
    \end{groupplot}
%     \path (top-|current bounding box.west)--
%           node[anchor=south,rotate=90] {Throughput (Operations per Second)}
%           (bot-|current bounding box.west);
% legend
\path (top|-current bounding box.north)--
      coordinate(legendpos)
      (bot|-current bounding box.north);
\matrix[
    matrix of nodes,
    anchor=south,
    draw,
    inner sep=0.2em,
    draw
  ]at([yshift=1ex]legendpos)
  {
    \ref{plots:lf-ravl}& lf-ravl&[5pt]
    \ref{plots:lf-ravl3}& lf-ravl3&[5pt]
    \ref{plots:lf-chrm}& lf-chrm&[5pt]
    \ref{plots:lf_chrm6}& lf-chrm6&[5pt]
    \ref{plots:lc-davl}& lc-davl&[5pt]
    \\};
\end{tikzpicture}
\centering
% \caption{Experiment results comparing lf-ravl and lf-ravl3 against
% self-balancing concurrent BSTs using randomly generated sequences under uniform
% distribution under different contention levels.}
% \label{fig:random_balanced_r4}
\end{figure}
Operation mix: $70\%$ {\getop}, $20\%$ {\insop} and $10\%$ {\delop}\\
High contention: key range $2\times 10^4$\\
Low contention: key range $2\times 10^6$

\end{frame}

\begin{frame}[fragile]
\frametitle{Performance under Different Update Ratios}
% Our solution scale better for write-dominant operation sequences compared to
% other concurrent self-balancing BSTs.
\begin{figure}
\begin{tikzpicture}
    \begin{groupplot}[group style={group size= 2 by 1},width=.45\textwidth]
        \nextgroupplot[title={Read-dominant},cycle list name=ravl_vs_balanced,
        smooth ,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}
    ] %r4 90
    \addplot coordinates {
(1, 5532134.97)
(2, 10117087.2)
(4, 19400121.8)
(8, 36076612.6)
(16, 65125568.69)
(32, 100642780.7)
    };
        \addplot coordinates {
(1, 5385604.8)
(2, 10005743)
(4, 19227700)
(8, 35732101.58)
(16, 64796641.4)
(32, 98487684.19)
    };
    \addplot coordinates {
(1, 5792479.6)
(2, 7440115)
(4, 16341901.02)
(8, 30577948.4)
(16, 60724184.16)
(32, 88070949.43)
    };
     \addplot plot coordinates {
(1, 5651897.8)
(2, 10628459.11)
(4, 20318457)
(8, 36421120.6)
(16, 66206312.87)
(32, 102085591.8)
    };
     \addplot plot coordinates {
(1, 5519175)
(2, 10257628.2)
(4, 19272733)
(8, 35605551.6)
(16, 65611983.61)
(32, 100811965.4)
    };
    \coordinate (top) at (rel axis cs:0,1);
    \nextgroupplot[title={Write-dominant},cycle list name=ravl_vs_balanced,
    smooth,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}] %r4 70
    \addplot coordinates {
(1, 3660247)
(2, 5123946)
(4, 9779694)
(8, 17880258)
(16, 32719535)
(32, 48319089)
    }; %chrm
    
    \addplot coordinates {
(1, 4064018)
(2, 6008887)
(4, 11371488)
(8, 20595519)
(16, 35964253)
(32, 57401712)
    }; %chrm6
    \addplot coordinates {
(1, 5106807)
(2, 4544188)
(4, 8476992)
(8, 16847068)
(16, 27531810)
(32, 40021738)
    }; %davl
    \addplot coordinates {
(1, 3927029)
(2, 5730954)
(4, 11005089)
(8, 20187832)
(16, 36142483)
(32, 53209197)
    }; %ravl
    
    \addplot coordinates {
(1, 4291128)
(2, 6335004)
(4, 11911736)
(8, 21937146)
(16, 40820432)
(32, 60760374)
    }; %ravl3

    \coordinate (bot) at (rel axis cs:1,0);% coordinate at bottom of the last plot
    \end{groupplot}
%     \path (top-|current bounding box.west)--
%           node[anchor=south,rotate=90] {Throughput (Operations per Second)}
%           (bot-|current bounding box.west);
% legend
\path (top|-current bounding box.north)--
      coordinate(legendpos)
      (bot|-current bounding box.north);
\matrix[
    matrix of nodes,
    anchor=south,
    draw,
    inner sep=0.2em,
    draw
  ]at([yshift=1ex]legendpos)
  {
    \ref{plots:lf-ravl}& lf-ravl&[5pt]
    \ref{plots:lf-ravl3}& lf-ravl3&[5pt]
    \ref{plots:lf-chrm}& lf-chrm&[5pt]
    \ref{plots:lf_chrm6}& lf-chrm6&[5pt]
    \ref{plots:lc-davl}& lc-davl&[5pt]
    \\};
\end{tikzpicture}
\centering
% \caption{Experiment results comparing lf-ravl and lf-ravl3 against
% self-balancing concurrent BSTs using randomly generated sequences under uniform
% distribution under different contention levels.}
% \label{fig:random_balanced_r4}
\end{figure}
Key range: $2\times 10^4$\\
Read-dominant: $90\%$ {\getop}, $9\%$ {\insop} and
$1\%$ {\delop}\\
Write-dominant: $50\%$ {\getop}, $25\%$ {\insop} and $25\%$
{\delop}

\end{frame}

\begin{frame}
\frametitle{Data Sequences with Different Degrees of Presortedness}
% Motivation:
% \begin{enumerate}
%   \item Experimental studies using random data favor BSTs without
%   self-balancing.
%   \item In many real-world applications, keys of data elements in an access
%     sequence are partially sorted ({\em presortedness})~\cite{Pfaff:2004}.
% \end{enumerate}
Presortedness of a data sequence ($n$ unique items):
\begin{enumerate}
  \item Measured by the number of {\em inversions}.
%   (the number of pairs of
%   numbers of the sequence in which the first item is larger than the second).
%   \item Generated using algorithm in~\cite{Elmasry:2005}.
  \item $m$: control the degree of presortedness.
  \item Number of inversions generated: $\frac{mn}{2}$.
\end{enumerate}
Sequence size: $2^{26}$.\\
$m$: $2^{9}$ - $2^{25}$.\\
{\insop}: in the order of the data sequences.\\
{\delop}/{\getop}: randomly.
\end{frame}



\begin{frame}[fragile]
\frametitle{Compare against Self-Balancing BSTs}
\begin{figure}

\begin{tikzpicture}
    \begin{groupplot}[group style={group size= 1 by 1},width=.45\textwidth]
        \nextgroupplot[xlabel={$\log m$},cycle list name=ravl_vs_balanced,
        smooth ,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}
    ] %r4 90
    \addplot coordinates {
(9, 16376392)
(13, 16004475)
(17, 15286072)
(21, 14426160)
(25, 13711255)
    };
        \addplot coordinates {
(9, 17444822)
(13, 16437093)
(17, 15666838)
(21, 14783587)
(25, 14556995)
    };
    \addplot coordinates {
(9, 16680822)
(13, 16235504)
(17, 15370998)
(21, 14616431)
(25, 14247242)
    };
     \addplot plot coordinates {
(9, 16168159)
(13, 15519227)
(17, 15003428)
(21, 14290065)
(25, 13562298)
    };
     \addplot plot coordinates {
(9, 17489409)
(13, 16492191)
(17, 15607620)
(21, 14806165)
(25, 14464572)
    };
    \coordinate (top) at (rel axis cs:0,1);

    \coordinate (bot) at (rel axis cs:1,0);% coordinate at bottom of the last plot
    \end{groupplot}
%     \path (top-|current bounding box.west)--
%           node[anchor=south,rotate=90] {Throughput (Operations per Second)}
%           (bot-|current bounding box.west);
% legend
\path (top|-current bounding box.north)--
      coordinate(legendpos)
      (bot|-current bounding box.north);
\matrix[
    matrix of nodes,
    anchor=south,
    draw,
    inner sep=0.2em,
    draw
  ]at([yshift=1ex]legendpos)
  {
    \ref{plots:lf-ravl}& lf-ravl&[5pt]
    \ref{plots:lf-ravl3}& lf-ravl3&[5pt]
    \ref{plots:lf-chrm}& lf-chrm&[5pt]
    \ref{plots:lf_chrm6}& lf-chrm6&[5pt]
    \ref{plots:lc-davl}& lc-davl&[5pt]
    \\};
\end{tikzpicture}
\centering
% \caption{Experiment results comparing lf-ravl and lf-ravl3 against
% self-balancing concurrent BSTs using randomly generated sequences under uniform
% distribution under different contention levels.}
% \label{fig:random_balanced_r4}
\end{figure}
Data sequence size: $2^{26}$ \\
Operation mix: $50\%$ {\getop}, $25\%$ {\insop} and $25\%$
{\delop}

\end{frame}
\begin{frame}
\frametitle{Compare against BSTs without Self-Balancing}
\begin{table}
\centering
\caption{Average tree heights using sequences with existing order under
different values of $m$.}
\label{tb:tree_heights}
\begin{tabular}{@{}ccccccc@{}}
\toprule
$\log m$ & Our solution & BSTs without
self-balancing
\\
\hline
11       & 38      & 10333           \\
15       & 37      & 177             \\
19       & 36      & 78              \\
23       & 35      & 70              \\
\hline
\bottomrule
\end{tabular}
\end{table}

\end{frame}
\begin{frame}[fragile]
\frametitle{Compare against BSTs without Self-Balancing (ctd.)}
\begin{figure}

\begin{tikzpicture}
    \begin{groupplot}[group style={group size= 1 by 1},width=.45\textwidth]
        \nextgroupplot[xlabel={$\log m$},cycle list name=ravl_vs_unbalanced,
        smooth ,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}
    ] %r4 90
    \addplot coordinates {
(9, 135267)
(13, 14492518)
(17, 17481542)
(21, 17042346)
(25, 16551424)
    };\label{plots:lf-nbst}
    \addplot coordinates {
(9, 102135)
(13, 12309317)
(17, 14128519)
(21, 12696814)
(25, 12202183)
    };\label{plots:lf-ebst}
    \addplot coordinates {
(9, 131160)
(13, 13401321)
(17, 19265709)
(21, 18216960)
(25, 17450461)
	};\label{plots:lf-ibst}
    \addplot plot coordinates {
(9, 131160)
(13, 15430843)
(17, 19103345)
(21, 18361957)
(25, 17332120)
    };\label{plots:lc-cast}
    \addplot plot coordinates {
(9, 122470)
(13, 9492352)
(17, 10460750)
(21, 9770278)
(25, 9791636)
    };\label{plots:lc-citr}
        \addplot plot coordinates {
(9, 17959181)
(13, 17818948)
(17, 17517134)
(21, 16521227)
(25, 16042963)
};%\label{plots:lf-ravl}
         \addplot plot coordinates {
(9, 18646823)
(13, 18258910)
(17, 18068697)
(21, 17334322)
(25, 16617788)
    };%\label{plots:lf-ravl}
    \coordinate (top) at (rel axis cs:0,1);

    \coordinate (bot) at (rel axis cs:1,0);% coordinate at bottom of the last plot
    \end{groupplot}
%     \path (top-|current bounding box.west)--
%           node[anchor=south,rotate=90] {Throughput (Operations per Second)}
%           (bot-|current bounding box.west);
% legend
\path (top|-current bounding box.north)--
      coordinate(legendpos)
      (bot|-current bounding box.north);
\matrix[
    matrix of nodes,
    anchor=south,
    draw,
    inner sep=0.2em,
    draw
  ]at([yshift=1ex]legendpos)
  {
    \ref{plots:lf-ravl}& lf-ravl&[5pt]
    \ref{plots:lf-ravl3}& lf-ravl3&[5pt]
    \ref{plots:lf-nbst}& lf-nbst&[5pt]
    \ref{plots:lf-ebst}& lf-ebst&[5pt] \\
    \ref{plots:lf-ibst}& lf-ibst&[5pt]
    \ref{plots:lc-cast}& lc-cast&[5pt]
    \ref{plots:lc-citr}& lc-citr&[5pt]
    \\};
\end{tikzpicture}
\centering
% \caption{Experiment results comparing lf-ravl and lf-ravl3 against
% self-balancing concurrent BSTs using randomly generated sequences under uniform
% distribution under different contention levels.}
% \label{fig:random_balanced_r4}
\end{figure}
Data sequence size: $2^{26}$ \\
Read-dominant: $90\%$ {\getop}, $9\%$ {\insop} and $1\%$
{\delop}

\end{frame}
% 
% \begin{frame}[fragile]
% \frametitle{Compare against BSTs without Self-Balancing (ctd.)}
% \begin{figure}
% 
% \begin{tikzpicture}
%     \begin{groupplot}[group style={group size= 1 by 1},width=.45\textwidth]
%         \nextgroupplot[xlabel={$\log m$}, cycle list name=ravl_vs_unbalanced,
%         smooth ,y tick label style={
%         /pgf/number format/.cd,
%             fixed,
%             fixed zerofill,
%             precision=1,
%         /tikz/.cd}
%     ] %r4 90
%     \addplot coordinates
%         {
% (9, 145465)
% (13, 13215382)
% (17, 17947195)
% (21, 16955846)
% (25, 16116545)
%     };%lfbst
% 
%     \addplot coordinates {
% (9, 103838)
% (13, 12434004)
% (17, 14048959)
% (21, 13295345)
% (25, 12793960)
%     };%2010
% 
%         \addplot coordinates {
% (9, 151302)
% (13, 13245410)
% (17, 18063631)
% (21, 17617860)
% (25, 16876753)
% };%ibst
% 
%     \addplot coordinates {
% (9, 154165)
% (13, 14804872)
% (17, 18346592)
% (21, 17722415)
% (25, 17169624)
%     };%cast
% 
%     \addplot coordinates {
% (9, 108912)
% (13, 6885706)
% (17, 10133626)
% (21, 9784162)
% (25, 9557006)
%     };%citr
%     \addplot coordinates {
% (9, 17119208)
% (13, 16578706)
% (17, 15963455)
% (21, 15271667)
% (25, 14657724)
%     };%ravl
%     \addplot coordinates {
% (9, 17742993)
% (13, 17340430)
% (17, 16334684)
% (21, 15920631)
% (25, 15463489)
%     };%ravl
%     \coordinate (top) at (rel axis cs:0,1);
% 
%     \coordinate (bot) at (rel axis cs:1,0);% coordinate at bottom of the last plot
%     \end{groupplot}
% %     \path (top-|current bounding box.west)--
% %           node[anchor=south,rotate=90] {Throughput (Operations per Second)}
% %           (bot-|current bounding box.west);
% % legend
% \path (top|-current bounding box.north)--
%       coordinate(legendpos)
%       (bot|-current bounding box.north);
% \matrix[
%     matrix of nodes,
%     anchor=south,
%     draw,
%     inner sep=0.2em,
%     draw
%   ]at([yshift=1ex]legendpos)
%   {
%     \ref{plots:lf-ravl}& lf-ravl&[5pt]
%     \ref{plots:lf-ravl3}& lf-ravl3&[5pt]
%     \ref{plots:lf-nbst}& lf-nbst&[5pt]
%     \ref{plots:lf-ebst}& lf-ebst&[5pt] \\
%     \ref{plots:lf-ibst}& lf-ibst&[5pt]
%     \ref{plots:lc-cast}& lc-cast&[5pt]
%     \ref{plots:lc-citr}& lc-citr&[5pt]
%     \\};
% \end{tikzpicture}
% \centering
% % \caption{Experiment results comparing lf-ravl and lf-ravl3 against
% % self-balancing concurrent BSTs using randomly generated sequences under uniform
% % distribution under different contention levels.}
% % \label{fig:random_balanced_r4}
% \end{figure}
% Data sequence size: $2^{26}$ \\
% Operation mix: $70\%$ {\getop}, $20\%$ {\insop} and $10\%$
% {\delop}
% 
% \end{frame}

\begin{frame}[fragile]
\frametitle{Compare against BSTs without Self-Balancing (ctd.)}
\begin{figure}

\begin{tikzpicture}
    \begin{groupplot}[group style={group size= 1 by 1},width=.45\textwidth]
        \nextgroupplot[xlabel={$\log m$}, cycle list name=ravl_vs_unbalanced,
        smooth ,y tick label style={
        /pgf/number format/.cd,
            fixed,
            fixed zerofill,
            precision=1,
        /tikz/.cd}
    ] %r4 90
    \addplot coordinates
        {
(9, 223377)
(13, 14438299)
(17, 17742017)
(21, 16782322)
(25, 15784344)
    };%bst

    \addplot coordinates {
(9, 170712)
(13, 12343986)
(17, 12986579)
(21, 12131180)
(25, 11651992)
    };%2010

        \addplot coordinates {
(9, 232997)
(13, 13238141)
(17, 18783078)
(21, 17573611)
(25, 17022577)
};%ibst

    \addplot coordinates {
(9, 229471)
(13, 14218723)
(17, 18114854)
(21, 17505773)
(25, 16935101)
    };%cast

    \addplot coordinates {
(9, 86579)
(13, 8314338)
(17, 8968368)
(21, 8759219)
(25, 8496388)
    }; %citr

    \addplot coordinates {
(9, 16168159)
(13, 15519227)
(17, 15003428)
(21, 14290065)
(25, 13562298)
    }; %ravl
    \addplot coordinates {
(9, 17489409)
(13, 16492191)
(17, 15607620)
(21, 14806165)
(25, 14464572)
    }; %ravl3
    \coordinate (top) at (rel axis cs:0,1);

    \coordinate (bot) at (rel axis cs:1,0);% coordinate at bottom of the last plot
    \end{groupplot}
%     \path (top-|current bounding box.west)--
%           node[anchor=south,rotate=90] {Throughput (Operations per Second)}
%           (bot-|current bounding box.west);
% legend
\path (top|-current bounding box.north)--
      coordinate(legendpos)
      (bot|-current bounding box.north);
\matrix[
    matrix of nodes,
    anchor=south,
    draw,
    inner sep=0.2em,
    draw
  ]at([yshift=1ex]legendpos)
  {
    \ref{plots:lf-ravl}& lf-ravl&[5pt]
    \ref{plots:lf-ravl3}& lf-ravl3&[5pt]
    \ref{plots:lf-nbst}& lf-nbst&[5pt]
    \ref{plots:lf-ebst}& lf-ebst&[5pt] \\
    \ref{plots:lf-ibst}& lf-ibst&[5pt]
    \ref{plots:lc-cast}& lc-cast&[5pt]
    \ref{plots:lc-citr}& lc-citr&[5pt]
    \\};
\end{tikzpicture}
\centering
% \caption{Experiment results comparing lf-ravl and lf-ravl3 against
% self-balancing concurrent BSTs using randomly generated sequences under uniform
% distribution under different contention levels.}
% \label{fig:random_balanced_r4}
\end{figure}
Data sequence size: $2^{26}$ \\
Write-dominant: $50\%$ {\getop}, $25\%$ {\insop} and $25\%$
{\delop}

\end{frame}


\begin{frame}
\frametitle{Conclusion}
% Our solution is potentially the best candidate for many real-world
% applications.
\begin{enumerate}
  \item Easy to implement in practice.
  \item Outperform other concurrent self-balancing BSTs.
  \item More suitable for many real-world applications compared to BSTs that do
  not support self-balancing.
\end{enumerate}

\end{frame}
\section{Acknowledgements}
\begin{frame}
\frametitle{Acknowledgements}
\begin{enumerate}
  \item Supervisor: Dr. Meng He
  \item Thesis committee: Dr. Andrew Rau-Chaplin and Dr. Alex Brodsky
  \item Chair of the defence: Dr. Michael Mcallister
\end{enumerate}
\end{frame}
\section{References}
\begin{frame}
\frametitle{References}
\footnotesize{
\begin{thebibliography}{99} % Beamer does not support BibTeX so references must be inserted manually as below
\bibitem[Brown:2013]{Brown:2013} 
Brown, Trevor and Ellen, Faith and Ruppert, Eric (2013)
\newblock Pragmatic Primitives for Non-blocking Data Structures
\newblock \emph{Proceedings of the 2013 ACM Symposium on Principles of
Distributed Computing.} 13--22.

\bibitem[Brown:2014]{Brown:2014} 
Brown, Trevor and Ellen, Faith and Ruppert, Eric (2014)
\newblock A General Technique for Non-blocking Trees
\newblock \emph{SIGPLAN Not.} 49(18), 329--342.

\bibitem[Sen:2010]{Sen:2010} 
Sen, Siddhartha and Tarjan, Robert E. (2010)
\newblock Deletion Without Rebalancing in Balanced Binary Trees
\newblock \emph{Proceedings of the Twenty-first Annual ACM-SIAM Symposium on
Discrete Algorithms.} 1490--1499.

\bibitem[Ramachandran:2015(1)]{Ramachandran:2015(1)} 
Ramachandran, Arunmoezhi and Mittal, Neeraj (2015)
\newblock A Fast Lock-Free Internal Binary Search Tree
\newblock \emph{Proceedings of the 2015 International Conference on Distributed
Computing and Networking.} 37:1--37:10.
\end{thebibliography}
}
\end{frame}

\begin{frame}
\frametitle{References}
\footnotesize{
\begin{thebibliography}{99} % Beamer does not support BibTeX so references must be inserted manually as below

\bibitem[Ramachandran:2015(2)]{Ramachandran:2015(2)}
Ramachandran, Arunmoezhi and Mittal, Neeraj (2015)
\newblock CASTLE: Fast Concurrent Internal Binary Search Tree Using Edge-based Locking
\newblock \emph{Proceedings of the 20th ACM SIGPLAN Symposium on Principles and
 Practice of Parallel Programming.} 281--282.

\bibitem[Arbel:2014]{Arbel:2014}
Arbel, Maya and Attiya, Hagit (2014)
\newblock Concurrent Updates with RCU: Search Tree As an Example
\newblock \emph{Proceedings of the 2014 ACM Symposium on Principles of Distributed Computing.} 196--205.

\bibitem[Natarajan:2014]{Natarajan:2014}
Natarajan, Aravind and Mittal, Neeraj (2014)
\newblock Fast Concurrent Lock-free Binary Search Trees
\newblock \emph{Proceedings of the 19th ACM SIGPLAN Symposium on Principles and Practice of Parallel Programming.} 1490--1499.

\bibitem[Ellen:2010]{Ellen:2010}
Ellen, Faith and Fatourou, Panagiota and Ruppert, Eric and van Breugel, Franck (2010)
\newblock Non-blocking Binary Search Trees
\newblock \emph{Proceedings of the 29th ACM SIGACT-SIGOPS Symposium on Principles of Distributed Computing.} 131--140.

\end{thebibliography}
}
\end{frame}

\begin{frame}
\frametitle{References}
\footnotesize{
\begin{thebibliography}{99}
\bibitem[Drachsler:2014]{Drachsler:2014}
Drachsler, Dana and Vechev, Martin and Yahav, Eran (2014)
\newblock Practical Concurrent Binary Search Trees via Logical Ordering
\newblock \emph{SIGPLAN Not..} 343--356.

\bibitem[Elmasry:2005]{Elmasry:2005}
Elmasry, Amr and Hammad, Abdelrahman (2005)
\newblock An Empirical Study for Inversions-Sensitive Sorting Algorithms
\newblock \emph{Experimental and Efficient Algorithms: 4th International Workshop} 597--601.

\bibitem[Pfaff:2004]{Pfaff:2004}
Pfaff, Ben (2004)
\newblock Performance Analysis of BSTs in System Software
\newblock \emph{Proceedings of the Joint International Conference on Measurement and Modeling of Computer Systems} 410--411.

\end{thebibliography}
}
\end{frame}
%------------------------------------------------

\begin{frame}
\Huge{\centerline{The End}}
\end{frame}

%----------------------------------------------------------------------------------------

\end{document} 